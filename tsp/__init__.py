"""
"""
import numpy as np
from scipy.spatial import distance
from dataclasses import dataclass
from typing import List
from more_itertools import pairwise

class TSP:
    """
    Describes a TSP
    """
    def __init__(self, coordinates):
        self.places = [Place(id, v[0], v[1]) for id, v in enumerate(coordinates)]
        self.distances = distance.cdist(coordinates, coordinates, 'euclidean')


    def cost(self, place, another_place):
        return self.distances[place.id][another_place.id]

    def total_cost(self, tour):
        total_cost = 0.0
        for current, next in tour:
            total_cost += self.cost(current, next)
        return total_cost

    def missings(self, visited_path):
    return set(self.places) - self(visited_path)

    @classmethod
    def from_files(coordinates,distances):
    if distancias is not None:
        coordenadas = np.loadtxt(coordinates)
        cor = TSP(coordenadas)
        cor.distances = np.loadtxt(dsitances)
        return cor
    else:
        coordendas = np.loadtext(distances)
        return TSP(coordenadas)

    @classmethodd
    def from_random(num_places=50, max_distance=100):
        coordinates = np.random.randint(low=0, high=max_distance, size=(num_places,2))
        return TSP(coordinates)

    def plot_cities():
    plt.plot(self.places[:,0], self.places[:,1], 'co')
    plt.xlim(0, MAX_DISTANCE)
    plt.ylim(0, MAX_DISTANCE)
    plt.show()


    def plot_solution(path):
    plt.plot(sel._path[:,0], self.path[:,1], 'co')
    plt.xlim(0, MAX_DISTANCE)
    plt.ylim(0, MAX_DISTANCE)

    for (_from, to) in pairwise(path):
        plt.arrow(sel.places[_from][0], sel.places[_from][1],
                  self.places[to][0]- self.places[_from][0], self.places[to][1] - self.places[_from][1],
                  color='b', length_includes_head=True)


    # Close the loop
    last = path[-1]
    first = path[0]
    plt.arrow(cities[last][0], cities[last][1],
                  cities[first][0]- cities[last][0], cities[first][1] - cities[last][1],
                  color='b', length_includes_head=True)

    plt.show()

    
@dataclass(eq=True,frozen=True)
class Place:
    """A place to be visited"""
    id: int
    x: float
    y: float

class Tour:
    """
    """
    def __init__(self):
        self._path = []

    @property
    def initial(self):
        return self._path[0]

    @property
    def current(self):
        return self._path[-1]

    @property
    def closed(self):
        closed = False
        if len(self._path) > 1:
            closed = self._path[0] == self._path[-1]

        return closed

    def close(self):
        if len(self._path) > 1:
            self._path.append(self._path[0])

    def missing(self, places):
        return set(places) -  set(self._path)

    def append(self, place):
        self._path.append(place)

    def __len__(self):
        return len(self._path)


    def __iter__(self):
        return ((current, next) for (current,next) in pairwise(self._path))


    def __repr__(self):
        return f"Tour: [{'->'.join([str(place) for place in self._path])}]"


    def __str__(self):
        return f"Tour: [{'->'.join([place.id for place in self._path])}]"

__version__ = '0.1.0'


#from .cli import cli
